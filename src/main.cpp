#include <iostream>
#include <cstdlib>
#include <vector>
#include <numeric>

#include "solution.h"

constexpr int BAR_WEIGHT = 20;

std::vector<int> AVAILABLE_PLATES {25, 20, 15, 10, 5};
std::vector<int> AVAILABLE_STRAPS {2, 2, 2, 2};

enum ErrorCodes : int
{
	SUCCESS = 0,
	NON_NUMERIC_WEIGHT = 1,
	LESS_THAN_BAR = 2,
	WEIGHT_TOO_HIGH = 3,
	NO_SOLUTIONS = 4,
};

int main(int argc, char** argv)
{
	std::cout << "Please enter a weight to load the barbell" << std::endl;

	int weight = 0;

	std::cin >> weight;

	if(std::cin.fail())
	{
		std::cout << "You have entered a none numerical value." << std::endl;
		return ErrorCodes::NON_NUMERIC_WEIGHT;
	}

	if(weight <= BAR_WEIGHT)
	{
		std::cout << "You have entered a value below the weight of the bar: " << BAR_WEIGHT << std::endl;
		return ErrorCodes::LESS_THAN_BAR;
	}

	if(weight % 2 != 0)
	{
		std::cout << "weight entered is not even, using " << --weight << std::endl;
	}

	const int totalWeightAvailable = 
			(std::accumulate(AVAILABLE_PLATES.begin(), AVAILABLE_PLATES.end(), 0) * 2)
		+ std::accumulate(AVAILABLE_STRAPS.begin(), AVAILABLE_STRAPS.end(), 0)
		+ BAR_WEIGHT;

	if(weight > totalWeightAvailable)
	{
		std::cout << "unfortunately, the weight is more than the plates allow, " << totalWeightAvailable << std::endl;
		return ErrorCodes::WEIGHT_TOO_HIGH;
	}

	const int weightToLoad = weight - BAR_WEIGHT;

	std::vector<BarbellLoading::Solution> solutions;
	BarbellLoading::FindAllSuitableSolutions(weightToLoad
			, AVAILABLE_PLATES
			, AVAILABLE_STRAPS
			, solutions);

	if(solutions.size() == 0)
	{
		std::cout << "unfortunately, could not find any solution to load this weight" << std::endl;
		return ErrorCodes::NO_SOLUTIONS;
	}

	for(const BarbellLoading::Solution& solution : solutions)
		std::cout << solution << std::endl;

	return ErrorCodes::SUCCESS;
}
