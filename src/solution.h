#pragma once

#include <vector>
#include <string>
#include <iostream>

namespace BarbellLoading
{

struct Solution
{
	std::vector<int> myPlates;
	std::vector<int> myStraps;
	int myWeightError;

	friend std::ostream& operator<<(std::ostream& os, const Solution& solution)
	{
		if(solution.myPlates.size() > 0)
		{
			os << "plates: [[ ";
			for(const int& plate : solution.myPlates)
			{
				os << plate << " ";
			}
			os << "]]" << std::endl;
		}

		if(solution.myStraps.size() > 0)
		{
			os << "straps: [[ ";
			for(const int& strap : solution.myStraps)
			{
				os << strap << " ";
			}
			os << "]]" << std::endl;
		}

		os << "weight Remaining: " << solution.myWeightError << std::endl;

		return os;
	}
};

void FindSuitablePlates(int aWeightPerSide
		, const std::vector<int>::const_iterator& aPlatesStart
		, const std::vector<int>::const_iterator& aPlatesEnd
		, std::vector<int>& someSelectedPlatesOut
		, int& aWeightRemainingPerSideOut)
{
	std::vector<int>::const_iterator plateIndex = aPlatesStart;

	while(aWeightPerSide > 0 && plateIndex != aPlatesEnd)
	{
		int plate = *plateIndex;
		if(aWeightPerSide >= plate)
		{
				someSelectedPlatesOut.push_back(plate);
				aWeightPerSide -= plate;
		}
		plateIndex++;
	}

	aWeightRemainingPerSideOut = aWeightPerSide;
}

void FindSuitableStraps(int aStrapWeight
		, const std::vector<int>& someAvailableStraps
		, std::vector<int>& someSelectedStrapsOut
		, int& aWeightRemainingOut)
{
	if(aStrapWeight > 0)
	{
		for(const int& strap : someAvailableStraps)
		{
			if(aStrapWeight >= strap)
			{
				someSelectedStrapsOut.push_back(strap);
				aStrapWeight -= strap;
			}

			if(aStrapWeight <= 0) break;
		}
	}

	aWeightRemainingOut = aStrapWeight;
}

void FindSuitableSolution(const int aWeight
		, const std::vector<int>::const_iterator& aPlatesStart
		, const std::vector<int>::const_iterator& aPlatesEnd
		, const std::vector<int>& someAvailableStraps
		, Solution& aSolutionOut)
{
	int weightPerSide = aWeight / 2;

	int weightPerSideRemaining = 0;

	FindSuitablePlates(weightPerSide
		, aPlatesStart
		, aPlatesEnd
		, aSolutionOut.myPlates
		, weightPerSideRemaining);

	int strapWeight = weightPerSideRemaining * 2;
	FindSuitableStraps(strapWeight
		, someAvailableStraps
		, aSolutionOut.myStraps
		, aSolutionOut.myWeightError);
}

void FindAllSuitableSolutions(const int aWeight
		, const std::vector<int>& someAvailablePlates
		, const std::vector<int>& someAvailableStraps
		, std::vector<Solution>& someSolutionsOut)
{
	for(auto iter = someAvailablePlates.begin(); iter != someAvailablePlates.end(); ++iter)
	{
		Solution solution;

		FindSuitableSolution(aWeight
				, iter
				, someAvailablePlates.end()
				, someAvailableStraps
				, solution);

		someSolutionsOut.push_back(solution);
	}
}

};
